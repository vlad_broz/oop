#pragma once
#include "Worker.h"

namespace Brozhinskii {
    class WriteFile : public Worker {
    private:
        std::string outputFile;
    public:
        WriteFile(const std::vector<std::string>& params);
        WorkerType getType() override;
        std::string execute(std::string& text) override;
    };
}


