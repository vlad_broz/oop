#include "../CPP_2/ReadFile.h"
#include "../CPP_2/Replace.h"
#include "../CPP_2/Sort.h"
#include "../CPP_2/WriteFile.h"
#include "../CPP_2/Grep.h"
#include "../CPP_2/Dump.h"
#include "Creator.h"

namespace Brozhinskii {
	static Creator<ReadFile> rfCreator("readfile");
	static Creator<Sort> sCreator("sort");
	static Creator<Replace> rpCreator("replace");
	static Creator<WriteFile> wfCreator("writefile");
	static Creator<Dump> dCreator("dump");
	static Creator<Grep> gCreator("grep");
}
