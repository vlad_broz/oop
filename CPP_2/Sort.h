#pragma once
#include "Worker.h"

namespace Brozhinskii {
    class Sort : public Worker {
    public:
        Sort() {}
        Sort(const std::vector<std::string>& params);
        WorkerType getType() override;
        std::string execute(std::string& text) override;
    };
}


