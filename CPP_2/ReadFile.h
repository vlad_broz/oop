#pragma once
#include <cstdio>
#include "Worker.h"

namespace Brozhinskii {
    class ReadFile : public Worker {
    private:
        std::string nameFile;
    public:
        ReadFile(const std::vector<std::string>& params);
        WorkerType getType() override;
        std::string execute(std::string& text) override;
    };
}
