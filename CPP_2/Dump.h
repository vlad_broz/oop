#pragma once
#include "Worker.h"

namespace Brozhinskii {
    class Dump : public Worker {
    private:
        std::string outputFile;
    public:
        Dump(const std::vector<std::string>& params);
        WorkerType getType() override;
        std::string execute(std::string& text) override;
    };
}
