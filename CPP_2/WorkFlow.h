#pragma once
#include <cstdio>
#include <string>
#include "Parser.h"
#include "../CPP_2/BlockFactory.h"

namespace Brozhinskii {
    class WorkFlow {
    public:
        void launch(std::string path);
    };
}
