#include <iostream>
#include "WorkFlow.h"

using namespace Brozhinskii;

int main() {
    WorkFlow workFlow;
    try {
        workFlow.launch("workflow.txt");
    }
    catch (std::string & exception) {
        std::cout << exception << endl;
    }
    return 0;
}
