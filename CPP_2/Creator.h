#pragma once
#include "CreatorInterface.h"
#include "BlockFactory.h"

namespace Brozhinskii {
    template<class C>
    class Creator : public CreatorInterface {
    public:
        Creator(const std::string& id) {
            BlockFactory::Instance().add(id, this);
        }
        std::shared_ptr<Worker> create(const std::vector<std::string>& params) const {
            return std::shared_ptr<Worker>{new C(params)};
        }
    };
}
