#pragma once
#include <memory>
#include "../CPP_2/Worker.h"

namespace Brozhinskii {
    class CreatorInterface {
    public:
        virtual std::shared_ptr<Worker> create(const std::vector<std::string>& params) const = 0;
    };
}
