#pragma once
#include "Worker.h"

namespace Brozhinskii {
    class Grep : public Worker {
    private:
        std::string word;
    public:
        Grep(const std::vector<std::string>& params);
        WorkerType getType() override;
        std::string execute(std::string& text) override;

    };
}
