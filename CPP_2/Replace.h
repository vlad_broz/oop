#pragma once
#include "Worker.h"

namespace Brozhinskii {
    class Replace : public Worker {
    private:
        std::string word1;
        std::string word2;
    public:
        Replace(const std::vector<std::string>& params);
        WorkerType getType() override;
        std::string execute(std::string& text) override;
    };
}

