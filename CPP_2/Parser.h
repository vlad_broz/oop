#pragma once
#include <cstdio>
#include <string>
#include <map>
#include <vector>
#include <fstream>
#include <sstream>

namespace Brozhinskii {
    using namespace std;
    typedef map<string, pair<string, vector<string>>> WorkFTable;
    class Parser {
    private:
        WorkFTable table;
        vector<string> order;

        bool is_number(const string& s);
    public:

        void parse(string& path);
        WorkFTable getTable();
        vector<string> getOrder();
    };
}
