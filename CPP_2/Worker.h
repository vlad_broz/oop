#pragma once
#include <string>
#include <vector>

namespace Brozhinskii {
    enum class WorkerType {
        IN,
        OUT,
        INOUT
    };

    class Worker {
    public:
        virtual ~Worker() = default;
        virtual WorkerType getType() = 0;
        virtual std::string execute(std::string& text) = 0;
    };
}

