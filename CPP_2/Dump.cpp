#include <fstream>
#include "Dump.h"

namespace Brozhinskii {
    Dump::Dump(const std::vector<std::string>& params) {
        if (params.size() != 1) throw std::string("Wrong input params");
        this->outputFile = params[0];
    }

    std::string Dump::execute(std::string& text) {
        std::ofstream fout(outputFile);
        fout << text;
        fout.close();
        return text;
    }

    WorkerType Dump::getType() {
        return WorkerType::INOUT;
    }
}
