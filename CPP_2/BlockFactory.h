#pragma once
#include <string>
#include <map>
#include "CreatorInterface.h"

namespace Brozhinskii {
    typedef std::map<std::string, CreatorInterface*> FactoryMap;

    class BlockFactory {
    protected:
        FactoryMap factory;
    public:
        void add(const std::string& id, CreatorInterface* creator);
        std::shared_ptr<Worker> create(const std::string& id, const std::vector<std::string>& params);
        static BlockFactory& Instance();
    };
}
