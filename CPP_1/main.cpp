#include <cstdlib>
#include <cstdio>
#include <iostream>
#include "Trit.h"
#include "TritSet.h"

using namespace Brozhinskii;

int main() {
	TritSet a(3), b(3);
	a[0] = Trit::TRUE;
	a[1] = Trit::FALSE;
	a[2] = Trit::TRUE;
	b[0] = Trit::UNKNOWN;
	b[1] = Trit::UNKNOWN;
	b[2] = Trit::FALSE;
	TritSet c = a & b;
	for (int i = 0; i < c.getSize(); i++) {
		std::cout << b[i];
	}

	system("pause");
}