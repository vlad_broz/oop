#include "TritSet.h"
#include "Constants.h"

namespace Brozhinskii {
	TritSet::TritSet(int size) : size(size), tritSet(1 + size / TRITS_IN_INT) {
		for (int i = 0; i < this->size; i++) {
			this->operator[](i) = Trit::UNKNOWN;
		}
	};

	Reference TritSet::operator[](int idx) {
		return Reference(idx, tritSet);
	}

	const Trit TritSet::operator[](int idx) const {
		int vec_idx = idx / (4 * sizeof(unsigned int));
		int int_idx = idx % (4 * sizeof(unsigned int));

		return Trit(TRIT_SAVE_VALUE & (tritSet[vec_idx] >> int_idx * BITS_PER_VALUE));
	}

	const TritSet TritSet::operator&(const TritSet& r) {
		int minSize = std::min(this->size, r.size),
			maxSize = std::max(this->size, r.size);
		TritSet c(maxSize);
		for (int i = 0; i < minSize; i++) {
			c[i] = this->operator[](i) & r[i];
		}
		if (this->size < r.size) {
			for (int i = minSize; i < maxSize; i++) {
				c[i] = Trit::UNKNOWN & r[i];
			}
		}
		else {
			for (int i = minSize; i < maxSize; i++) {
				c[i] = this->operator[](i) & Trit::UNKNOWN;
			}
		}
		return c;
	}

	const TritSet TritSet::operator|(const TritSet& r) {
		int minSize = std::min(this->size, r.size),
			maxSize = std::max(this->size, r.size);
		TritSet c(maxSize);
		for (int i = 0; i < minSize; i++) {
			c[i] = this->operator[](i) | r[i];
		}
		if (this->size > r.size) {
			for (int i = minSize; i < maxSize; i++) {
				c[i] = this->operator[](i) | Trit::UNKNOWN;
			}
		}
		else {
			for (int i = minSize; i < maxSize; i++) {
				c[i] = r[i] | Trit::UNKNOWN;
			}
		}
		return c;
	}

	const TritSet TritSet::operator~() {
		TritSet c(this->size);
		for (int i = 0; i < this->size; i++) {
			c[i] = (~this->operator[](i));
		}
		return c;
	}

	int TritSet::getSize() {
		return this->size;
	}

	size_t TritSet::length() {
		int i = this->size - 1;
		while (this->operator[](i) == Trit::UNKNOWN && i > 0) {
			i--;
		}
		return i == 0 ? i : i + 1;
	}

	size_t TritSet::capacity() {
		return this->tritSet.capacity();
	}

	const size_t TritSet::cardinality(Trit value) {
		size_t count = 0;
		int length = this->length();
		for (int i = 0; i < this->size && i < length; i++) {
			if (this->operator[](i) == value)
				count++;
		}
		return count;
	}

	std::unordered_map<Trit, int, std::hash<int>> TritSet::cardinality() {
		std::unordered_map<Trit, int, std::hash<int>> map;
		map[Trit::TRUE] = 0;
		map[Trit::FALSE] = 0;
		map[Trit::UNKNOWN] = 0;
		int length = this->length();
		for (int i = 0; i < this->size && i < length; i++) {
			map[this->operator[](i)]++;
		}
		return map;
	}

	void TritSet::trim(size_t lastIndex) {
		if (lastIndex <= this->size) {
			for (int i = lastIndex; i < this->size; i++) {
				this->operator[](i) = Trit::UNKNOWN;
			}
			int cap = (this->length() / TRITS_IN_INT) + 1;
			if (cap < this->tritSet.capacity()) {
				this->tritSet.resize(cap);
				this->tritSet.shrink_to_fit();
				this->size = this->tritSet.capacity() * TRITS_IN_INT;
			}
		}
	}
}


