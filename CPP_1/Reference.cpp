#include "Reference.h"

namespace Brozhinskii {
	Reference::Reference(int idx, std::vector<unsigned int>& tritSet): idx(idx), tritSet(tritSet) {}

	Reference& Reference::operator=(Trit trit) {
		int vec_idx = idx / (4 * sizeof(unsigned int));
		int int_idx = idx % (4 * sizeof(unsigned int));

		tritSet[vec_idx] &= ~(TRIT_SAVE_VALUE << int_idx * BITS_PER_VALUE);
		tritSet[vec_idx] |= (trit << int_idx * BITS_PER_VALUE);
	
		return *this;
	}

	Reference& Reference::operator=(const Reference& reference) {
		*this = Trit(reference);
		return *this;
	}

	Reference::operator Trit() const {
		int vec_idx = idx / (4 * sizeof(unsigned int));
		int int_idx = idx % (4 * sizeof(unsigned int));

		return Trit(TRIT_SAVE_VALUE & (tritSet[vec_idx] >> int_idx * BITS_PER_VALUE));
	}
}


