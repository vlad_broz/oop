#include "Trit.h"

namespace Brozhinskii {
	Trit operator&(const Trit& lhs, const Trit& rhs) {
		return Trit((int)lhs & (int)rhs);
	}

	Trit operator|(const Trit& lhs, const Trit& rhs) {
		return Trit((int)lhs | (int)rhs);
	}

	Trit operator~(const Trit& rhs) {
		if (rhs == Trit::FALSE)
			return Trit::TRUE;
		if (rhs == Trit::TRUE)
			return Trit::FALSE;
		return Trit::UNKNOWN;
	}
}

