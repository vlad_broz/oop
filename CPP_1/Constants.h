#pragma once

const unsigned int TRITS_IN_INT = 4 * sizeof(unsigned int);
const int BITS_PER_VALUE = 2;
const int TRIT_SAVE_VALUE = 3;