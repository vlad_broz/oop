#pragma once

namespace Brozhinskii {
	enum Trit : unsigned int {TRUE = 3, FALSE = 0, UNKNOWN = 1};

	Trit operator&(const Trit& lhs, const Trit& rhs);

	Trit operator|(const Trit& lhs, const Trit& rhs);

	Trit operator~(const Trit& rhs);
}
