#include "pch.h"
#include "../Trit.h"
#include "../TritSet.h"

using namespace Brozhinskii;

namespace {
	const size_t RAND_SIZE = 24000;

	class RandomTritSet {
	public:
		const size_t SIZE;
		Trit* arr;
		TritSet set;

		RandomTritSet(size_t size = RAND_SIZE): SIZE(size), set(size) {
			Trit values[3] = {Trit::TRUE, Trit::FALSE, Trit::UNKNOWN};
			arr = new Trit[SIZE];
			for (size_t i = 0; i < SIZE; i++) {
				arr[i] = values[rand() % 3];
				set[i] = arr[i];
			}
		}

		~RandomTritSet() {
			delete[] arr;
		}
	};
}

namespace ConstructorAndAssigningTest {
	TEST(ConstructorAndAssigningTest, ConstructorTest) {
		TritSet set(24);
		for (int i = 0; i < 24; i++) {
			set[i] = Trit::FALSE;
		}
		ASSERT_EQ(set.getSize(), 24);
		EXPECT_EQ(set.capacity(), 2);
		for (int i = 0; i < 24; i++) {
			ASSERT_EQ(Trit(set[i]), Trit::FALSE);
		}
	}
	TEST(ConstructorAndAssigningTest, AssigningOperatorTest) {
		RandomTritSet randSet;

		randSet.set[6] = Trit::TRUE;
		if (true) {
			TritSet set2(RAND_SIZE);
			set2 = randSet.set;
			for (int i = 0; i < randSet.SIZE; i++) {
				ASSERT_EQ(Trit(set2[i]), randSet.set[i]);
			}
		}
		ASSERT_EQ(Trit(randSet.set[6]), Trit::TRUE);
	}
}

namespace SizeChangingTest {
	TEST(SizeChangingTest, LengthTest) {
		TritSet set(40);
		
		ASSERT_EQ(0, set.length());

		set[5] = Trit::TRUE;
		ASSERT_EQ(6, set.length());

		set[24] = Trit::FALSE;
		ASSERT_EQ(25, set.length());
	}

	TEST(SizeChangingTest, TrimTest) {
		TritSet set(20);
		for (int i = 0; i < 15; i++) {
			set[i] = Trit::TRUE;
		}

		set.trim(30);
		ASSERT_EQ(set.length(), 15);

		set.trim(10);
		ASSERT_EQ(set.length(), 10);
		ASSERT_EQ(set.capacity(), 1);
	}

	TEST(SizeChangingTest, CardinalityFirstTest) {
		RandomTritSet randomTritSet;
		std::unordered_map< Trit, int, std::hash<int> > test_map;

		for (size_t i = randomTritSet.SIZE; i > randomTritSet.SIZE / 2; i--) {
			randomTritSet.set[i] = Trit::UNKNOWN;
		}
		for (size_t i = 0; i < randomTritSet.set.length(); i++) {
			test_map[randomTritSet.arr[i]]++;
		}

		std::unordered_map< Trit, int, std::hash<int> > res_map = randomTritSet.set.cardinality();
		ASSERT_EQ(test_map[Trit::TRUE], res_map[Trit::TRUE]);
		ASSERT_EQ(test_map[Trit::FALSE], res_map[Trit::FALSE]);
		ASSERT_EQ(test_map[Trit::UNKNOWN], res_map[Trit::UNKNOWN]);
	}

	TEST(SizeChangingTest, CardinalitySecondTest) {
		RandomTritSet randomTritSet;
		int countT = 0, countF = 0, countU = 0;

		for (size_t i = randomTritSet.SIZE; i > randomTritSet.SIZE / 2; i--) {
			randomTritSet.set[i] = Trit::UNKNOWN;
		}
		for (size_t i = 0; i < randomTritSet.set.length(); i++) {
			if (randomTritSet.set.operator[](i) == Trit::TRUE)
				countT++;
			if (randomTritSet.set.operator[](i) == Trit::FALSE)
				countF++;
			if (randomTritSet.set.operator[](i) == Trit::UNKNOWN)
				countU++;
		}

		ASSERT_EQ(countT, randomTritSet.set.cardinality(Trit::TRUE));
		ASSERT_EQ(countF, randomTritSet.set.cardinality(Trit::FALSE));
		ASSERT_EQ(countU, randomTritSet.set.cardinality(Trit::UNKNOWN));
	}
}

namespace ReferenceTest {
	TEST(ReferenceTest, ReferenceGetTest) {
		RandomTritSet randomTritSet;

		for (size_t i = 0; i < randomTritSet.SIZE; i -= -1) {
			ASSERT_EQ(Trit(randomTritSet.set[i]), randomTritSet.arr[i]);
		}
	}

	TEST(ReferenceTest, ReferenceSetTest) {
		TritSet set(10);
		for (int i = 0; i < set.getSize(); i++) {
			set[i] = Trit::FALSE;
		}

		set[4] = Trit::TRUE;
		ASSERT_EQ(Trit(set[4]), Trit::TRUE);

		set[5] = Trit::TRUE;
		ASSERT_EQ(Trit(set[5]), Trit::TRUE);

		ASSERT_TRUE(set[5] == set[4]);
	}
}

namespace TritOperatorsTest {
	TEST(TritOperatorsTest, AndOperatorTest) {
		EXPECT_EQ(Trit::TRUE & Trit::TRUE, Trit::TRUE);
		EXPECT_EQ(Trit::FALSE & Trit::FALSE, Trit::FALSE);
		EXPECT_EQ(Trit::UNKNOWN & Trit::UNKNOWN, Trit::UNKNOWN);
		EXPECT_EQ(Trit::TRUE & Trit::FALSE, Trit::FALSE);
		EXPECT_EQ(Trit::TRUE & Trit::UNKNOWN, Trit::UNKNOWN);
		EXPECT_EQ(Trit::FALSE & Trit::UNKNOWN, Trit::FALSE);
	}

	TEST(TritOperatorsTest, OrOperatorTest) {
		EXPECT_EQ(Trit::TRUE | Trit::TRUE, Trit::TRUE);
		EXPECT_EQ(Trit::FALSE | Trit::FALSE, Trit::FALSE);
		EXPECT_EQ(Trit::UNKNOWN | Trit::UNKNOWN, Trit::UNKNOWN);
		EXPECT_EQ(Trit::TRUE | Trit::FALSE, Trit::TRUE);
		EXPECT_EQ(Trit::TRUE | Trit::UNKNOWN, Trit::TRUE);
		EXPECT_EQ(Trit::FALSE | Trit::UNKNOWN, Trit::UNKNOWN);
	}

	TEST(TritOperatorsTest, NotOperatorTest) {
		EXPECT_EQ(~Trit::TRUE, Trit::FALSE);
		EXPECT_EQ(~Trit::FALSE, Trit::TRUE);
		EXPECT_EQ(~Trit::UNKNOWN, Trit::UNKNOWN);
	}
}

namespace TritSetOperatorsTest {
	TEST(TritSetOperatorsTest, AndOperatorTest) {
		RandomTritSet right(20000), left(10000);
		TritSet res = right.set & left.set;
		for (size_t i = 0; i < 10000; i++)
		{
			ASSERT_EQ(Trit(res[i]), right.arr[i] & left.arr[i]);
		}
		for (size_t i = 10000; i < 20000; i++)
		{
			ASSERT_EQ(Trit(res[i]), right.arr[i] & Trit::UNKNOWN);
		}
	}
	TEST(TritSetOperatorsTest, OrOperatorTest) {
		RandomTritSet right(20000), left(10000);
		TritSet res = right.set | left.set;
		for (size_t i = 0; i < 10000; i++)
		{
			ASSERT_EQ(Trit(res[i]), right.arr[i] | left.arr[i]);
		}
		for (size_t i = 10000; i < 20000; i++)
		{
			ASSERT_EQ(Trit(res[i]), right.arr[i] | Trit::UNKNOWN);
		}
	}
	TEST(TritSetOperatorsTest, NotOperatorTest) {
		RandomTritSet left(10000);
		TritSet res = ~left.set;
		for (size_t i = 0; i < 10000; i++)
		{
			ASSERT_EQ(Trit(res[i]), ~left.arr[i]);
		}
	}

}