#pragma once
#include <vector>
#include "Reference.h"
#include <algorithm>
#include <unordered_map>
#include "Constants.h"

namespace Brozhinskii {
	class TritSet {
	private:
		int size;
		std::vector<unsigned int> tritSet;
	public:
		TritSet(int size);

		Reference operator[](int idx);
		const Trit operator[](int idx) const;
		const TritSet operator&(const TritSet& r);
		const TritSet operator|(const TritSet& r);
		const TritSet operator~();

		int getSize();
		size_t length();
		size_t capacity();
		const size_t cardinality(Trit value);
		std::unordered_map< Trit, int, std::hash<int> > cardinality();
		void trim(size_t lastIndex);
	};
}