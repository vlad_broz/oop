#pragma once
#include <vector>
#include "Trit.h"
#include "Constants.h"

namespace Brozhinskii  {
	class TritSet;
	class Reference {
		friend class TritSet;
	private:
		int idx;
		std::vector<unsigned int>& tritSet;
		Reference(int idx, std::vector<unsigned int>& tritSet);
	public:
		Reference& operator=(Trit trit);
		Reference& operator=(const Reference& reference);
		operator Trit() const;
	};
}



