#pragma once
#include <fstream>
#include "WordsStat.h"
#include "CsvFormater.h"

namespace Brozhinsky {
	class Reporter {
	private:
		WordsStat& wordsStat;
		std::ofstream& out;

	public:
		Reporter(WordsStat& wordsStat, std::ofstream& out);
		void getReport();
	};
}

