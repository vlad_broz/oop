#include "CsvFormater.h"

namespace Brozhinsky {
	namespace {
		const std::string COMMA = ", ";
		const std::string PERCENT = "%";
	}

	CsvFormater::CsvFormater(WordsStat& wordsStat) : wordsStat(wordsStat) {}

	std::string CsvFormater::output() {
		std::string notes;
		std::string str;
		wordsStat.sortMapByFrequency();
		for (auto pair : wordsStat.getNotes()) {
			notes += pair.first + COMMA + std::to_string(pair.second) + COMMA
				+ std::to_string((double)pair.second * 100 / wordsStat.getSize()) + PERCENT + "\n";
		}
		return notes;
	}
}