#pragma once
#include <string>
#include <fstream>
#include "WordsStat.h"

namespace Brozhinsky {
	class Reader {
	private:
		std::ifstream& in;
		WordsStat& wordsStat;
	public:
		Reader(std::ifstream& in, WordsStat& wordsStat);
		void read();
	};
}
