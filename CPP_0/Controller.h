#pragma once
#include <fstream>

namespace Brozhinsky {
	class Controller {
	private:
		std::ifstream& in;
		std::ofstream& out;
	public:
		Controller(std::ifstream& in, std::ofstream& out);
		void process();
	};
}
