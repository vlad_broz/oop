#include "Reader.h"
#include "StringSplitter.h"

namespace Brozhinsky {
	Reader::Reader(std::ifstream& in, WordsStat& wordsStat) : in(in), wordsStat(wordsStat) {}

	void Reader::read() {
		StringSplitter stringSplitter(wordsStat);
		std::string str;

		while (in) {
			std::getline(in, str);
			stringSplitter.splitString(str);
		}
	};
}
