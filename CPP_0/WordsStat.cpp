#include "WordsStat.h"

namespace Brozhinsky {
	WordsStat::WordsStat() {
		size = 0;
	}
	void WordsStat::addNote(std::string str) {
		wordsMap[str]++;
		size++;
	}

	std::map<std::string, int> WordsStat::getMap() {
		return wordsMap;
	}

	std::vector<std::pair<std::string, int>> WordsStat::getNotes() {
		return notes;
	}

	int WordsStat::getSize() {
		return size;
	}

	bool  comp(std::pair<std::string, int> a, std::pair<std::string, int> b) {
		return a.second > b.second;
	}

	void WordsStat::sortMapByFrequency() {
		for (auto pair : wordsMap) {
			notes.push_back(pair);
		}
		std::sort(notes.begin(), notes.end(), comp);
	}
}

