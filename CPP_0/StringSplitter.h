#pragma once
#include "WordsStat.h"
#include <string>

namespace Brozhinsky {
	class StringSplitter {
	private:
		WordsStat& wordsStat;
		bool getSymbolType(char a);

	public:
		StringSplitter(WordsStat& wordsStat);
		void splitString(std::string str);
	};
}
