#pragma once
#include <map>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>        

namespace Brozhinsky {
	class WordsStat {
	private:
		std::map<std::string, int> wordsMap;
		std::vector<std::pair<std::string, int>> notes;
		int size;
	public:
		WordsStat();
		void addNote(std::string str);
		std::map<std::string, int> getMap();
		std::vector<std::pair<std::string, int>> getNotes();
		int getSize();
		void sortMapByFrequency();
	};
}

