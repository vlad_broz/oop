#pragma once
#include <string>
#include "WordsStat.h"

namespace Brozhinsky {
	class CsvFormater {
	private:
		WordsStat& wordsStat;

	public:
		CsvFormater(WordsStat& wordsStat);
		std::string output();
	};
}
