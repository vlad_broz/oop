#include "Controller.h"
#include "Reader.h"
#include "Reporter.h"
#include "WordsStat.h"

namespace Brozhinsky {

	Controller::Controller(std::ifstream &in, std::ofstream &out): in(in), out(out){}

	void Controller::process() {
		WordsStat wordsStat;
		Reader reader(in, wordsStat);
		reader.read();
		Reporter reporter(wordsStat, out);
		reporter.getReport();
	}
}


