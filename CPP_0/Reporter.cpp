#include "Reporter.h"

namespace Brozhinsky {
	const std::string HEADER_CSV = "Word, Frequency, Percentage Frequency\n";

	Reporter::Reporter(WordsStat& wordsStat, std::ofstream& out) :
		wordsStat(wordsStat), out(out) {}

	void Reporter::getReport() {
		CsvFormater formater(wordsStat);
		out << HEADER_CSV;
		out << formater.output();
	}
}