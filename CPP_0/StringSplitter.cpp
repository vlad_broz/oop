#include "StringSplitter.h"

namespace Brozhinsky {
	bool StringSplitter::getSymbolType(char a) {
		if ((a >= 'a' && a <= 'z') || (a >= 'A' && a <= 'Z') || (a >= '0' && a <= '9')
			|| (a >= '�' && a <= '�') || (a >= '�' && a <= '�')) {
			return 1;
		}
		return 0;
	}

	StringSplitter::StringSplitter(WordsStat& wordsStat) : wordsStat(wordsStat) {}

	void StringSplitter::splitString(std::string str) {
		std::string buff;
		for (char a : str) {
			if (getSymbolType(a)) {
				buff += a;
			}
			else if (buff != "") {
				wordsStat.addNote(buff);
				buff = "";
			}
		}
		if (buff != "") {
			wordsStat.addNote(buff);
			buff = "";
		}
	}
}