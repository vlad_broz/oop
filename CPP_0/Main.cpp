#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "Controller.h"

const int FIRST_PROGRAM_ARGUMENT = 1;
const int SECOND_PROGRAM_ARGUMENT = 2;

int main(int argv, char** argc) {
	char* InFileName = argc[FIRST_PROGRAM_ARGUMENT];
	char* OutFileName = argc[SECOND_PROGRAM_ARGUMENT];
	std::ifstream in;
	std::ofstream out;

	in.open(InFileName);
	if (!in) {
		std::cout << "Input file error!";
		return -1;
	}
	out.open(OutFileName);

	Brozhinsky::Controller controller(in, out);
	controller.process();
	in.close();
	out.close();
}